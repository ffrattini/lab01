package lab01.example.model;

public class SimpleBankAccountWithATM extends SimpleBankAccount{

    public SimpleBankAccountWithATM(AccountHolder holder, final double balance) {
        super(holder, balance);
    }

    public void depositFromATM(int userID, double amount) {
        deposit(userID, amount - 1);
    }

    public void withdrawFromATM(int userID, double amount) {
        withdraw(userID, amount + 1);
    }

}
