import lab01.example.model.AccountHolder;
import lab01.example.model.SimpleBankAccountWithATM;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class SimpleBankAccountWithATMTest {
    private AccountHolder accountHolder;
    private SimpleBankAccountWithATM bankAccount;

    @BeforeEach
    void beforeEach() {
        accountHolder = new AccountHolder("Maria", "Bianchi", 0);
        bankAccount = new SimpleBankAccountWithATM(accountHolder, 0);
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, accountHolder.getId());
        assertEquals(0, bankAccount.getBalance());
    }

    @Test
    void testDepositFromATM() {
        bankAccount.deposit(accountHolder.getId(), 500);
        bankAccount.depositFromATM(accountHolder.getId(), 50);
        assertEquals(549, bankAccount.getBalance());
        assertNotEquals(550, bankAccount.getBalance());
    }

    @Test
    void testWrongDepositFromATM() {
        bankAccount.depositFromATM(accountHolder.getId(), 100);
        bankAccount.depositFromATM(3, 300);
        assertEquals(99, bankAccount.getBalance());
    }

    @Test
    void testWithdrawFromATM() {
        bankAccount.deposit(accountHolder.getId(), 500);
        bankAccount.depositFromATM(accountHolder.getId(), 50);
        bankAccount.withdraw(accountHolder.getId(), 50);
        bankAccount.withdrawFromATM(accountHolder.getId(), 100);
        assertEquals(398, bankAccount.getBalance());
    }

    @Test
    void testWrongWithdrawFromATM() {
        bankAccount.deposit(accountHolder.getId(), 500);
        bankAccount.depositFromATM(accountHolder.getId(), 50);
        bankAccount.withdrawFromATM(accountHolder.getId(), 550);
        assertEquals(549, bankAccount.getBalance());
    }
}
