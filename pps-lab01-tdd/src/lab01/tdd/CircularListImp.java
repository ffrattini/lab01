package lab01.tdd;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CircularListImp implements CircularList {

    private List<Integer> circularList = new ArrayList<>();
    private int index = 0;

    @Override
    public void add(int element) {
        circularList.add(element);
    }

    @Override
    public int size() {
        return circularList.size();
    }

    @Override
    public boolean isEmpty() {
        return circularList.isEmpty();
    }

    @Override
    public Optional<Integer> next() {
        if (index >= circularList.size()) {
            index = 0;
        }
        if (size() < 1)
            return Optional.empty();
        return Optional.of(circularList.get(index++));
    }

    @Override
    public Optional<Integer> previous() {
        if (index == 0) {
            index = size();
        }
        if (size() < 1)
            return Optional.empty();
        return Optional.ofNullable(circularList.get(--index));
    }

    @Override
    public void reset() {
        index = 0;
    }

    @Override
    public Optional<Integer> next(SelectStrategy strategy) {
        if(circularList.isEmpty())
           return Optional.empty();
        int elementsChecked = 0;
        while (elementsChecked < size()) {
            int value = next().get();
            if (strategy.apply(value))
                return Optional.of(value);
            elementsChecked++;
        }
        return Optional.empty();
    }

}
