package lab01.tdd;

/*
select the next equal element of the given one
 */
public class EqualsStrategy implements SelectStrategy {

    private int value;

    public EqualsStrategy (int value) {
        this.value = value;
    }

    @Override
    public boolean apply(int element) {
        return element == this.value;
    }
}
