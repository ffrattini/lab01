package lab01.tdd;

/*
Select the next even element of the list
 */
public class EvenStrategy implements SelectStrategy {

    @Override
    public boolean apply(int element) {
        return element % 2 == 0;
    }
}
