package lab01.tdd;

/*
select the next equal element of the given one
 */
public class MultipleOfStrategy implements SelectStrategy {

    private int value;

    public MultipleOfStrategy(int value) {
        this.value = value;
    }

    @Override
    public boolean apply(int element) {
        return element % value == 0;
    }
}
