package lab01.tdd;

/*
select the next odd element of the list
 */
public class OddStrategy implements SelectStrategy {

    @Override
    public boolean apply(int element) {
        return element % 2 != 0;
    }

}
