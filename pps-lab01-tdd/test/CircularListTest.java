import lab01.tdd.CircularList;
import lab01.tdd.CircularListImp;
import org.junit.jupiter.api.*;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the CircularList implementation
 */
public class CircularListTest {

    private final CircularList circularList = new CircularListImp();

    private void initializeList() {
        circularList.add(15);
        circularList.add(12);
        circularList.add(4);
        circularList.add(66);
        circularList.add(101);
    }

    @Test
    public void testEmptyList(){
        assertTrue(circularList.isEmpty());
        assertThrows(NoSuchElementException.class, () -> {
                    circularList.next().get();
                });
    }

    @Test
    public void testAddElements() {
        circularList.add(3);
        assertEquals(3, circularList.next().get());
    }

    @Test
    public void testNext() {
        initializeList();
        assertEquals(15, circularList.next().get());
        assertEquals(12, circularList.next().get());
        assertEquals(4, circularList.next().get());
        assertEquals(66, circularList.next().get());
        circularList.reset();
        assertEquals(15, circularList.next().get());
    }

    @Test
    public void testPrevious() {
        initializeList();
        assertTrue(circularList.previous().isPresent());
        assertEquals(66, circularList.previous().get());
    }

    @Test
    public void testOneElement() {
        circularList.add(11);
        assertEquals(1, circularList.size());
        assertEquals(11, circularList.next().get());
        assertEquals(11, circularList.next().get());
        assertEquals(11, circularList.next().get());
        assertEquals(11, circularList.previous().get());
        assertEquals(11, circularList.previous().get());
    }

    @Test
    public void testSize() {
        assertEquals(0, circularList.size());
        initializeList();
        assertEquals(5, circularList.size());
    }

}
