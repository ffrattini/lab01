import lab01.tdd.*;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

public class SelectStrategyTest {
    private final CircularListImp circularList = new CircularListImp();

    private void initializeList() {
        circularList.add(24);
        circularList.add(31);
        circularList.add(55);
        circularList.add(6);
        circularList.add(101);
    }

    @Test
    public void testEmptyList(){
        assertTrue(circularList.isEmpty());
        assertThrows(NoSuchElementException.class, () -> {
            circularList.next().get();
        });
    }

    @Test
    public void testEvenStrategy() {
        assertThrows(NoSuchElementException.class, () -> {
            circularList.next(new EvenStrategy()).get();
        });
        initializeList();
        assertFalse(circularList.isEmpty());
        assertEquals(24, circularList.next(new EvenStrategy()).get());
        assertEquals(6, circularList.next(new EvenStrategy()).get());
    }

    @Test
    public void testOddStrategy() {
        initializeList();
        assertEquals(31, circularList.next(new OddStrategy()).get());
        assertEquals(55, circularList.next(new OddStrategy()).get());
        assertEquals(101, circularList.next(new OddStrategy()).get());
        assertEquals(31, circularList.next(new OddStrategy()).get());
    }

    @Test
    public void testMultipleStrategy() {
        assertThrows(NoSuchElementException.class, () -> {
            circularList.next(new MultipleOfStrategy(9)).get();
        });
        initializeList();
        assertEquals(55, circularList.next(new MultipleOfStrategy(11)).get());
        assertThrows(NoSuchElementException.class, () -> {
            circularList.next(new MultipleOfStrategy(13)).get();
        });
        assertEquals(24, circularList.next(new MultipleOfStrategy(8)).get());
    }

    @Test
    public void testEqualsStrategy() {
        initializeList();
        assertEquals(31, circularList.next(new EqualsStrategy(31)).get());
        assertThrows(NoSuchElementException.class, () -> {
            circularList.next(new EqualsStrategy(2)).get();
        });
        circularList.add(2);
        assertEquals(2, circularList.next(new EqualsStrategy(2)).get());
        assertEquals(55, circularList.next(new EqualsStrategy(55)).get());
    }
}
